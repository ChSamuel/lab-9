from datetime import datetime
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout

def index(request):
    return render(request, 'index.html')

def login(request):
    if(request.user is not None):
        if request.user.is_authenticated:
            return redirect('index')
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(
            request, username=username, password=password
        )
        if user is not None:
            auth_login(request, user)
            request.session['login_timestamp'] = str(datetime.now())
            return redirect('index')

    return render(request, 'login.html')

def logout(request):
    auth_logout(request)
    return redirect('index')