from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.contrib.auth import login
from django.http import HttpRequest
from .views import *

class Lab9Test(TestCase):
    @classmethod
    def setUpClass(cls):
        super(Lab9Test, cls).setUpClass()
        cls.user = User.objects.create_user(
            'test', 'test@gmail.com', 'katasandi'
        )

    def test_index_not_authenticated(self):
        response = self.client.get('/')
        self.assertIn("You aren't logged in yet", response.content.decode())
    
    def test_index_is_authenticated(self):
        self.client.login(username='test', password='katasandi')
        response = self.client.get('/')
        self.assertIn('test', response.content.decode())
        response = self.client.get('/login')
        self.assertTrue(response.status_code, 302)
    
    def test_login_exists(self):
        response = self.client.get('/login')
        self.assertTrue(response.status_code, 200)
    
    def test_login_is_authenticated(self):
        request = HttpRequest()
        request.user = User()
        login(request)
    
    def test_login_post_fail(self):
        request = HttpRequest()
        request.method = 'POST'
        request.user = None
        request.POST['username'] = "Tester"
        request.POST['password'] = "katasandi"
        login(request)
    
    def test_logout(self):
        self.client.login(username='test', password='katasandi')
        response = self.client.get('/')
        self.assertIn('test', response.content.decode())
        response = self.client.get('/logout')
        self.assertTrue(response.status_code, 302)
    
    @classmethod
    def tearDownClass(cls):
        super(Lab9Test, cls)